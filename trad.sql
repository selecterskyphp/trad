-- phpMyAdmin SQL Dump
-- version 4.1.4
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 2014-10-22 12:31:44
-- 服务器版本： 5.6.16
-- PHP Version: 5.5.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `trad`
--

-- --------------------------------------------------------

--
-- 表的结构 `tbl_activity`
--

DROP TABLE IF EXISTS `tbl_activity`;
CREATE TABLE IF NOT EXISTS `tbl_activity` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `addtime` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `tbl_admin`
--

DROP TABLE IF EXISTS `tbl_admin`;
CREATE TABLE IF NOT EXISTS `tbl_admin` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `pass` varchar(128) NOT NULL,
  `logintime` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- 转存表中的数据 `tbl_admin`
--

INSERT INTO `tbl_admin` (`id`, `name`, `pass`, `logintime`) VALUES
(1, 'admin', '21232f297a57a5a743894a0e4a801fc3', 0);

-- --------------------------------------------------------

--
-- 表的结构 `tbl_people`
--

DROP TABLE IF EXISTS `tbl_people`;
CREATE TABLE IF NOT EXISTS `tbl_people` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `sex` tinyint(1) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `tbl_people_relate`
--

DROP TABLE IF EXISTS `tbl_people_relate`;
CREATE TABLE IF NOT EXISTS `tbl_people_relate` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `peopleid` int(10) unsigned NOT NULL,
  `name` varchar(20) NOT NULL,
  `qq` varchar(20) NOT NULL,
  `tel` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `tbl_subterm`
--

DROP TABLE IF EXISTS `tbl_subterm`;
CREATE TABLE IF NOT EXISTS `tbl_subterm` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `actid` int(10) unsigned NOT NULL,
  `termid` int(10) unsigned NOT NULL,
  `title` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `tbl_term`
--

DROP TABLE IF EXISTS `tbl_term`;
CREATE TABLE IF NOT EXISTS `tbl_term` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `actid` int(10) unsigned NOT NULL,
  `title` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `tbl_trad`
--

DROP TABLE IF EXISTS `tbl_trad`;
CREATE TABLE IF NOT EXISTS `tbl_trad` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `actid` int(10) unsigned NOT NULL,
  `termid` int(10) unsigned NOT NULL,
  `subtermid` int(10) unsigned NOT NULL,
  `peopleid` int(10) unsigned NOT NULL,
  `peoplesubid` int(10) unsigned NOT NULL,
  `addtime` int(10) unsigned NOT NULL,
  `ip` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
