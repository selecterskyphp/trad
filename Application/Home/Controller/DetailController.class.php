<?php
// 本类由系统自动生成，仅供测试用途
namespace Home\Controller;

use Home\Controller;

class DetailController extends Base {
    function __construct()
    {
        parent::__construct();

    }
    public function index()
    {  
        $this->assign('DETAIL_ON',' class="active"');
        $this->assign('TITLE','产品详细 - '.$this->WEB_NAME);        
        $this->display();
    }    
}