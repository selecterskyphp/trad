<?php
// 本类由系统自动生成，仅供测试用途
namespace Admin\Controller;


class IndexController extends Base {

    public function test()
    {
        $this->display();
    }

    public function index(){

        $this->assign('HOME_ON',' class="active"');
        $this->assign('TITLE','首页'.$this->title); 
	    $this->display();
    }


    public  function login()
    {
        $this->display();
    }

    public function doLogin()
    {
        $this->dao = M('Admin');
        $username = I('post.name');
        $pwd = I('post.pwd');
        if(!$username || !$pwd)
        {
            $this->error('请输入正确登录的信息');
        }
        $row = $this->dao->getByName($username);
        if(!$row || !is_array($row))
        {
            $this->error('用户名不存在');
        }
        if($row['pass'] != md5($pwd))
        {
            $this->error('密码不正确');
        }
        $this->uid = $row['id'];
        $this->username = $row['name'];

        $this->setLoginStatus(true);
        $this->assign('jumpUrl',U('Index/index'));
        $this->success('登录成功！');
    }

    public function logout()
    {
        $this->setLoginStatus(false);
        $this->redirect('Index/login');
    }
}