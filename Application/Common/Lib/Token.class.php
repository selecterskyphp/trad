<?php
/**
 * Created by IntelliJ IDEA.
 * User: appie
 * Date: 14-4-30
 * Time: 下午2:18
 */

namespace Common\Lib;

/*
Token管理类 1）指定表名  2）表名必须有如下字段： uid int(11)、token varchar(128)、addtime int(11)
*/
//token有效时间为10分钟，10内没有任何操作，则视为token过期
define('TOKEN_INVAILD_TIME', 60*10);

class Token {

    private $tablename;    
    private $dao;
    private $timeout;

    //$timeout为过期时间 单位为秒 默认为10分钟
    function __construct($timeout=0,$tablename='Token')
    {
        $this->timeout = $timeout;
        $this->tablename = $tablename;
        $this->dao = M($this->tablename);
        if($this->timeout === 0)
        {
            $this->timeout = TOKEN_INVAILD_TIME;
        }
    }

    //验证token
    public function verify($token)
    {
        $time = time();
        //自动清除过期的token
        $where = 'addtime+'.$this->timeout.'<'.$time;
        $this->dao->where($where)->delete();
        $where = array('token'=>$token);
        $row = $this->dao->where($where)->find();
        if(!$row)
        {
            return false;
        }
        
        $data = array();
        $data['addtime'] = $time;            
        //更新token
        $this->dao->data($data)->where($where)->save();
        return $row['uid'];
    }

    //创建一个新的token
    public function createToken($uid)
    {
        $time = time();
        $rnd = rand_string().' '.$time;
        $token = $uid.'-'.md5($rnd);

        $data = array('token'=>$token,'uid'=>$uid,'addtime'=>$time);
        $re = $this->dao->data($data)->add();
        if(!$re)
        {
            return false;
        }
        return $token;      
    }

    public function delToken($token)
    {
        $where = array('token'=>$token);
        $this->dao->where($where)->delete();
    }
}
