<?php
/**
 * Created by IntelliJ IDEA.
 * User: appie
 * Date: 14-4-30
 * Time: 上午10:04
 */
namespace Common\Lib;

class BaiduApi {

    private $token;
    private $sk;
    private $ak;
    private $appid;

    //媒体通信url api
    private $URL = "https://pcs.baidu.com/rest/2.0/pcs/device";
    //登录请求url api
    private $BAIDU_AUTHORIZE_URL = 'https://openapi.baidu.com/oauth/2.0/authorize?';
    //二次验证请求url api
    private $BAIDU_TONKEN_URL = 'https://openapi.baidu.com/oauth/2.0/token';
    //获取用户信息url api
    private $BAIDU_USERINFO_URL= 'https://openapi.baidu.com/rest/2.0/passport/users/getLoggedInUser';

    function __construct($config='')
    {
        $initConfig = array('appid'=>'1537715','ak'=>'6lhla07u6h5qbVT8zQmNntUZ','sk'=>'6FIC8vKh99d1llhPxuVpvFdszc3Qv0GL');

        if(!is_array($config))
        {
            $config = $initConfig;
        }
        else
        {
            $config = array_merge($initConfig,$config);
        }
        $this->appid = $config['appid'];
        $this->ak = $config['ak'];
        $this->sk = $config['sk'];
    }

    public function login($redirect_uri,$authCode = '')
    {
        if(!$authCode)
        {
            $auth_url = $this->BAIDU_AUTHORIZE_URL;
            $auth_url .= 'response_type=code&client_id=' . $this->ak;
            $auth_url .= '&redirect_uri=' . urlencode($redirect_uri);
            $auth_url .= '&scope=basic%20netdisk%20super_msg&confirm_login=1';
            $auth_url .= '&display=page';
            //die($auth_url);
            //$this->redirect($auth_url);
            @header('Location: ' . $auth_url);
            exit;
        }
        else
        {
            $param = array(
                'grant_type' => 'authorization_code',
                'code' => $authCode,
                'client_id' => $this->ak,
                'client_secret' => $this->sk,
                'redirect_uri' => $redirect_uri
            );

            $result = $this->sendRequest($param,$this->BAIDU_TONKEN_URL,false,true);
            if($result['code'] !== 0)
            {
                $result['message'] = '请求session失败:'.$result['message'];
                return $result;
            }
            $this->token = $result['data']['access_token'];
            //$refresh_token = $result['data']['refresh_token'];
            return $result;
        }
    }
    public function setToken($token)
    {
        $this->token = $token;
    }

    public function getSK()
    {
        return $this->sk;
    }
    public function getAK()
    {
        return $this->ak;
    }
    public function getAPPID()
    {
        return $this->appid;
    }

    public function getToken()
    {
        return $this->token;
    }
    public function userInfo()
    {
        $param = array('access_token'=>$this->token);
        $result = $this->sendRequest($param, $this->BAIDU_USERINFO_URL, false, true);
        return $result;
    }

    public function registerDevice($deviceid_, $desc_)
    {
        $param = array(
            'method'=>'register',
            'device_type'=>1,
            'deviceid'=>$deviceid_,
            'desc'=>$desc_);
        $result = $this->sendRequest($param);
        return $result;
    }

    public function updateDevice($deviceid_,$desc)
    {
        $param = array('method'=>'update','deviceid'=>$deviceid_,'desc'=>$desc);
        return $this->sendRequest($param);
    }

    public function delDevice($deviceid_)
    {
        $param = array('method'=>'drop','deviceid'=>$deviceid_);
        return $this->sendRequest($param);
    }

    public function queryDevice()
    {
        $param = array('method'=>'list','device_type'=>1);
        return $this->sendRequest($param);
    }

    public function shareDevice($deviceid_)
    {
        $param = array('method'=>'createshare','deviceid'=>$deviceid_,'share'=>1);
        return $this->sendRequest($param);
    }
    public function unShareDevice($deviceid_)
    {
        $param = array('method'=>'cancelshare','deviceid'=>$deviceid_);
        return $this->sendRequest($param);
    }

    public function queryShareDevice($start,$limit)
    {
        $time = time();
        $expire = $time + 100;
        $realSign = $this->appid.$expire.$this->ak.$this->sk;
        $realSign = md5($realSign);
        $sign = $this->appid.'-'.$this->ak.'-'.$realSign;
        $param = array('method'=>'listshare','sign'=>$sign,'expire'=>$expire,'start'=>$start,'end'=>$limit);
        //查询公共设备不发送token
        return $this->sendRequest($param, $this->URL, false);

    }
    public function subscribe($sherid, $uk)
    {
        $param = array('method'=>'subscribe','shareid'=>$sherid,'uk'=>$uk);
        return $this->sendRequest($param);
    }

    public function unSubscribe($shareid, $uk)
    {
        $param = array('method'=>'unsubscribe','shareid'=>$shareid,'uk'=>$uk);
        return $this->sendRequest($param);
    }
    public function querySubscribe()
    {
        $param = array('method'=>'listsubscribe');
        return $this->sendRequest($param);
    }
    public function startLive($deviceid_,$type='')
    {
        $param = array('method'=>'liveplay','deviceid'=>$deviceid_,'type'=>$type);
        return $this->sendRequest($param);
    }
    public function startLiveByShare($shareid, $uk,$type='')
    {
        $param = array('method'=>'liveplay','shareid'=>$shareid, 'uk'=>$uk,'type'=>$type);
        return $this->sendRequest($param);
    }
    public function queryReplay($deviceid_,$starttime_,$endtime_)
    {
        $param = array('method'=>'playlist','deviceid'=>$deviceid_,'st'=>$starttime_,'et'=>$endtime_);
        return $this->sendRequest($param);
    }
    public function uriForReplay($deviceid_,$starttime_,$endtime_)
    {
        $url = $this->URL."?method=vod&access_token=".$this->token."&deviceid=".$deviceid_."&st=".$starttime_."&et=".$endtime_;
        return $url;
    }
    public function queryLastThumbail($deviceid_)
    {
        $param = array('method'=>'thumbnail','deviceid'=>$deviceid_,'latest'=>1);
        $result = $this->sendRequest($param);
        $url = '';
        if($result['code'] == 0)
        {
            $url = $result['data']['list'][0]['url'];
        }
        return $url;
    }
    public function control($deviceid_, $data)
    {
        $param = array('method'=>'control','deviceid'=>$deviceid_,'command'=>$data);
        $result = $this->sendRequest($param);
        
        return $result;
    }
    private function sendRequest($param,$url = '',$token=true,$post=false)
    {
        $errorType = C('JSON_ERROR_TYPE');

        if($token)
        {
            $param['access_token'] = $this->token;
        }
        if(!$url)
        {
            $url = $this->URL;
        }
      // var_dump($url,$param);
      // exit;
        $result = array();
        if($post === true)
        {
            $result = do_post($url,$param);
        }
        else
        {
            $result = do_get($url,$param);
        }
        if($result['code'] !== 0)
        {
            $data = errorByCode($result['code'],$result['message'],$errorType['BAIDU']);
            return $data;
        }
        if($result['http_code'] != 200 || $result['data']['error_code'] != 0)
        {
            $data = errorByCode($result['data']['error_code'],$result['data']['error_msg'],$errorType['BAIDU']);           
            return $data;
        }
        $data = successByData($result['data']);
        return $data;
    }



}
