<?php

$database = require ('./config.php');

$sysconfig = array(
    'WEB_NAME'=>'Trad',
    'WEB_DONAME'=>'localhost',
    //远程请求json返回格式 code>0表示操作失败 code保存相应的错误码
    'JSON_RESPONSE_MODEL' => array('code'=>0,'message'=>'','data'=>array(),'errorType'=>0), 
    //数据校验密钥
    'MD5_KEY'=>'aabbcced'
);
$config	= array(
    'DEFAULT_THEME'		=> '',
    'DEFAULT_CHARSET' => 'utf-8',
    'APP_GROUP_LIST' => 'Home,Admin',
    'DEFAULT_GROUP' =>'Home',
    'TMPL_FILE_DEPR' => '_',
    'DB_FIELDS_CACHE' => false,
    'DB_FIELDTYPE_CHECK' => true,
   // 'URL_ROUTER_ON' => true,
    //'URL_MODEL'          => '0',
    'URL_HTML_SUFFIX' =>'',
    'SESSION_AUTO_START'=>true,
    'DEFAULT_LANG'   => 'cn',
    'LANG_SWITCH_ON'		=> true,
    'LANG_LIST'=>'cn,zh-cn,en',
    'TAGLIB_LOAD' => true,
    'LAYOUT_ON'=>true,    
   // 'TMPL_ACTION_ERROR' => APP_PATH.'/Tpl/Home/Default/Public/success.html',
  //  'TMPL_ACTION_SUCCESS' =>  APP_PATH.'/Tpl/Home/Default/Public/success.html',
    'COOKIE_PREFIX'=>'trad_',
    'COOKIE_EXPIRE'=>'',
//    'VAR_PAGE' => 'p',
//    'LAYOUT_HOME_ON'=>$sys_config['LAYOUT_ON'],
//    'URL_ROUTE_RULES' => $RULES,
//    'TMPL_EXCEPTION_FILE' => APP_PATH.'/Tpl/Home/Default/Public/exception.html'
);
return array_merge($database, $config,$sysconfig);
