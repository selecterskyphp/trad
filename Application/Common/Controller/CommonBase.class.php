<?php
/**
 * Created by IntelliJ IDEA.
 * User: appie
 * Date: 14-4-29
 * Time: 下午2:24
 */

namespace Common\Controller;

use Think\Controller;

class CommonBase extends Controller
{
    protected $WEB_NAME;
    protected $WEB_DOMAIN;
    protected $dao;

    function __construct()
    {
        parent::__construct();

        $this->WEB_NAME = C('WEB_NAME');
        $this->WEB_DOMAIN = C("WEB_DOMAIN");

        $this->assign('__ACTION_NAME__',ACTION_NAME);
        $this->assign('__MOUDLE_NAME__',CONTROLLER_NAME);       

        //echo '__construct,'.ACTION_NAME.','.CONTROLLER_NAME.','.$Think['THEME_NAME'].','.THEME_PATH;
    }
} 
